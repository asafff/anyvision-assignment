# *Anyvision Video Resize Tool*
## Requirements
Please use a Windows machine.

Please make sure you have Python(3.7), ERLang(22.1) & RabbitMQ(3.80) Server Pre Installed.

[ERLang Download Page](https://www.erlang.org/downloads)

[RabbitMQ Server Download Link](https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.0/rabbitmq-server-3.8.0.exe)

Install external libraries by running the requirements file:
```
pip install -r requirements.txt
```

## Running Celery Background Worker
After you've installed all the dependencies and you have RabbitMQ Server running, you can start the celery workers.
```
celery worker -A tasks -l INFO -P gevent -n my_worker1 -Q queue1 --without-gossip -f FILEPATH
```
*NOTE:* Please make sure the logs path already exists. FILEPATH example:
```
celery worker -A tasks -l INFO -P gevent -n my_worker1 -Q queue1 --without-gossip -f D:\vids\logs\celery.logs
```
You can run as many workers as you'd like (limited by your system resources), to be able to process multiple jobs at once. Make sure to change the worker name argument.

## Sending Jobs
When your worker is up and running, you can start sending jobs to it.
The default requested criterias were assigned to *test.py*

*test.py* requires 3 command line arguments, and won't run without them:

1. n(int) = Number of video instances

2. m(int) = Seconds to wait between instances launch

3. vid_dir(str) = Videos directory. Folders separated by a backward slash. Don't surround it by quotation marks

For example, a correct command would be:

```
python test.py 3 10 D:\vids\
```

Wrong commands that will result in an error:

```
python test.py
python test.py 3 10 - Missing vid_dir argument.
python test.py A 10 D:\vids\ - A character instead of int.
python test.py 3 A D:\vids\ - A character instead of int.
python test.py 3 10 D:/vids/ - Forward slashes instead of backward slashes.
python test.py 3 10 D:\\vids\\ - Double backward slashes instead one between folders.
python test.py 3 10 D:\vids - Without last slash which goes into the directory.
python test.py D:\vids\ 3 10 - Wrong order of the arguments.
```