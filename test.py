from tasks import *
from datetime import datetime, timedelta
from pytz import timezone
import os, sys

# Commands to use before running test file
# celery worker -A tasks -l INFO -P gevent -n my_worker1 -Q queue1 -f D:\vids\logs\celery.logs
# celery worker -A tasks -l INFO -P gevent -n my_worker2 -Q queue1 -f D:\vids\logs\celery.logs

if len(sys.argv) != 4:
    print('Wrong number of command line parameters.\n\
Please input 3 arguments as follows:\n\
  1. n = Number of video instances\n\
  2. m = Seconds to wait between instances launch\n\
  3. vid_dir = Videos directory. Folders separated by backwards slashes. Don\'t surround it by quotation marks\n\
For example:\n\
  test.py 3 10 D:\\vids\\')
    sys.exit(-1)

# Number of video instances
n = int(sys.argv[1])

# Set timezone to avoid wrong time runtime
my_tz = timezone('Israel')

# Used time spans declarations
now = my_tz.localize(datetime.now())
m = timedelta(seconds=int(sys.argv[2]))
m_sec = now + m
m_sec_edited = m_sec

# Define video directory, and get a list of all the videos inside the directory
vid_dir = sys.argv[3]
videos = [f for f in os.listdir(vid_dir) if os.path.isfile(os.path.join(vid_dir, f))]

# Loop through the videos, and run 'main' on each. passing the full video dir as an argument, and ensuring correct queue
for i, video in zip(range(n), videos):
    full_vid_dir = os.path.join(vid_dir, video)
    if i > 0:
        main.apply_async(args=[full_vid_dir, vid_dir], queue="queue1", eta=m_sec_edited)
        m_sec_edited +=  m
    else:
        main.apply_async(args=[full_vid_dir, vid_dir], queue="queue1")

