from __future__ import absolute_import, unicode_literals
from celery import Celery
import os
import numpy as np
from celery.utils.log import get_task_logger
import time
import cv2

app = Celery()
logger = get_task_logger(__name__)


# Create vid instance path
def create_vid_dir(vid_dir):
    if not os.path.exists(vid_dir + "tmp\\"):
        os.mkdir(vid_dir + "tmp\\")

    vid_instances = 0
    while True:
        tmp_path = vid_dir + "tmp\\vid-instance%d\\" % vid_instances
        if not os.path.exists(tmp_path):
            os.mkdir(tmp_path)
            logger.info('Path created: ' + tmp_path)
            return tmp_path
        vid_instances += 1


# Split vid to jpg frames, to resize it
def split_vid_to_frames(video, tmp_path):
    logger.info('Started to resize video frames')
    start_time = time.time()
    vidcap = cv2.VideoCapture(video)
    success, image = vidcap.read()
    count = 0
    while success:
        height, width, layers = image.shape
        new_h = height // 2
        new_w = width // 2
        resize = cv2.resize(image, (new_w, new_h))
        cv2.imwrite("%s%05d.jpg" % (tmp_path, count), resize)
        count += 1
        success, image = vidcap.read()
        if not success:
            end_time = time.time()
            delta = end_time - start_time
            average = delta / count
            logger.info('Processed %i frames in an average of %f' % (count, average))

@app.task
def main(full_vid_dir, vid_dir):
    tmp_path = create_vid_dir(vid_dir)
    split_vid_to_frames(full_vid_dir, tmp_path)
    return
